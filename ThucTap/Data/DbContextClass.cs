﻿using ThucTap.Models;
using Microsoft.EntityFrameworkCore;

namespace ThucTap.Data
{
    public class DbContextClass : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DbContextClass(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        }

        public DbSet<ProductDetails> Products { get; set; }
        public DbSet<OrderDetails> Orders { get; set; }
        public DbSet<CustomerDetails> Customers { get; set; }
    }
}
